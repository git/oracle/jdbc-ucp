# This is an OLD Repository. Check out our [New Repository](https://github.com/oracle/oracle-db-examples/tree/master/java).

This is an old repository. The code samples related to Java Database Connecitivity (JDBC), Universal Connection Pool (UCP), and Java in the Database (OJVM) are available and maitained on a [new Github Repository](https://github.com/oracle/oracle-db-examples/tree/master/java).

Please refer to the new repository and subscribe to be on the watch list to get notified when the new code samples are added. 




